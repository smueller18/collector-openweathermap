FROM ucalgary/python-librdkafka:3.7.0-0.11.6

COPY requirements.txt /app/

RUN apk add --no-cache --virtual .build-deps gcc musl-dev libffi-dev libressl-dev && \
    pip install --no-cache-dir -r /app/requirements.txt && \
    apk del --no-cache .build-deps

ENV LOGGING_FORMAT "%(levelname)8s %(asctime)s %(name)s [%(filename)s:%(lineno)s - %(funcName)s() ] %(message)s"

COPY ./collector/ /app/

CMD ["python", "/app/collector.py"]
