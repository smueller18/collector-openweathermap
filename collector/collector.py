#!/usr/bin/env python

import requests
import os
import logging

from kafka_connector.avro_loop_producer import AvroLoopProducer
from kafka_connector.timer import Unit

__author__ = u'Stephan Müller'
__copyright__ = u'2019, Stephan Müller'
__license__ = u'MIT'

__dirname__ = os.path.dirname(os.path.abspath(__file__))

logging.basicConfig(level=logging.INFO, format=os.getenv("LOGGING_FORMAT"))
logger = logging.getLogger('collector')


PUSH_INTERVAL = int(os.getenv("PUSH_INTERVAL", "60"))
KAFKA_HOSTS = os.getenv("KAFKA_HOSTS", "kafka:9092")
SCHEMA_REGISTRY_URL = os.getenv("SCHEMA_REGISTRY_URL", "http://schema-registry:8081")
TOPIC = "api.openweathermap.org.weather"

API_KEY = os.getenv("API_KEY", "")
LOCATION_IDS = os.getenv("LOCATION_IDS", "2892794")
API_BASE_URL = "https://api.openweathermap.org/data/2.5"

KEY_SCHEMA = __dirname__ + "/config/key.avsc"
VALUE_SCHEMA = __dirname__ + "/config/value.avsc"


def get_data():
    """Retrieve weather data from openweathermap API."""
    data = list()
    for location_id in LOCATION_IDS.split(","):
        response = requests.get(f'{API_BASE_URL}/weather?id={location_id}&APPID={API_KEY}')
        if response.status_code != 200:
            raise ConnectionError(f"API request was not successful. Code {response.status_code}: {response.text}")
        response = response.json()
        data.append({
            'key': {
                'location_id': response['id']
            },
            'value': {
                'timestamp': response['dt'] * 1000,
                'temperature': response['main']['temp'] - 273.15,
                'wind_speed': response['wind']['speed'],
                'wind_direction': response['wind']['deg'] if 'deg' in response['wind'] else -1,
                'pressure': response['main']['pressure'],
                'humidity': response['main']['humidity'],
                'location_name': response['name'],
            },
            'timestamp': response['dt'] * 1000
        })
    return data


producer = AvroLoopProducer(KAFKA_HOSTS, SCHEMA_REGISTRY_URL, TOPIC, KEY_SCHEMA, VALUE_SCHEMA)

producer.loop(get_data, interval=PUSH_INTERVAL, unit=Unit.SECOND)
